#!/usr/bin/env python
from constructs import Construct
from cdktf import App, TerraformStack
from imports.aws import Instance

class MyStack(TerraformStack):
    def __init__(self, scope: Construct, ns: str):
        super().__init__(scope, ns)

        demoRunnerInstance = Instance(self, 'demo-runner-instance',
          ami="ami-04d29b6f966df1537",
          instance_type="t2.small",
          vpc = Token().as_string(demoRunnerVPC.vpc_id_output)
        )


app = App()
MyStack(app, "source")

app.synth()

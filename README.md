# Ideamia Developer Workflow
## Prerequisites
* Terraform >= v0.14.0
* CDKTF >= 0.0.18
* Python >= 3.8
* Pip >= 9.0.1